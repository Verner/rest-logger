#include "rest_logger.h"

#include "cpr/cpr.h"
#include <boost/algorithm/string/join.hpp>

#include <iostream>

using namespace verner::logging;

RESTLogger::RESTLogger ( const verner::logging::Config& cfg ):
    log_queue(new moodycamel::ConcurrentQueue<std::string>),
    stop_scheduled(false),
    log_server_url(""),
    log_server_user(""),
    log_server_password("")
{
    log_server_url = cfg.at("url");
    if(auto user = cfg.find("user"); user != cfg.end())
        log_server_user = user->second;
    if(auto pass = cfg.find("password"); pass != cfg.end())
        log_server_password = pass->second;
    if(auto workers = cfg.find("workers"); workers != cfg.end())
        NUM_WORKERS = std::stoi(workers->second);
    if(auto max_mem = cfg.find("max_mem"); max_mem != cfg.end())
        MAX_MEM_SIZE = std::stoi(max_mem->second);
    if(auto async_post = cfg.find("async"); async_post != cfg.end())
        ASYNC_POST = (async_post->second != "false");

    if (log_server_url.rfind("file://")==0)
        log_file.open(log_server_url.substr(7), std::ios_base::app);
    for(int i=0; i<NUM_WORKERS; i++)
        worker_threads.push_back(std::thread([this](){ process_queue(); }));
}

auto msecs() {
    return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
}


RESTLogger::~RESTLogger()
{
    std::cout << "Flushing " << log_queue->size_approx() << " items" << std::endl;
    auto t_start = msecs();
    stop_scheduled = true;
    for(auto& worker: worker_threads) {
        worker.join();
    }
    delete log_queue;
    auto t_end = msecs();
    std::cout << "Flushed in " << t_end - t_start << "ms" << std::endl;
}

void RESTLogger::process_item ( const std::string& json_log_item )
{
    post_request("[" + json_log_item + "]", true);
}

void RESTLogger::post_request ( const std::string& body, bool force_async )
{
    if (log_file.is_open()) {
        log_file << body << std::endl;
    } else if (ASYNC_POST || force_async) {
            cpr::PostAsync(
                cpr::Url{log_server_url},
                cpr::Body{body},
                cpr::Header({{"Content-Type", "application/json"}}),
                cpr::Authentication{log_server_user, log_server_password}
            );
    } else {
            cpr::Post(
                cpr::Url{log_server_url},
                cpr::Body{body},
                cpr::Header({{"Content-Type", "application/json"}}),
                cpr::Authentication{log_server_user, log_server_password}
            );
    }
}


void RESTLogger::process_queue()
{
    std::string log_items[BATCH_SIZE];

    while (! stop_scheduled) {
        while ( size_t count  = log_queue->try_dequeue_bulk(log_items, BATCH_SIZE) ) {
            auto t_start = msecs();
            std::string json = "[";
            for(int i=0;i<count-1;++i) {
                json += log_items[i] + ",";
            }
            json += log_items[count-1] + "]";
            post_request(json);
            auto t_end = msecs();
            approx_queue_size -= (json.size() - 2 - count);
//             std::cout << std::this_thread::get_id() <<"-> processed " << count << " in " << t_end-t_start << "ms" << std::endl;
        }
//         std::cout << std::this_thread::get_id() <<"-> sleeping" << std::endl;
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
//     std::cout << std::this_thread::get_id() <<"-> quitting" << std::endl;
}

