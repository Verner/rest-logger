#ifndef JSON_H_INCLUDED
#define JSON_H_INCLUDED

#include <chrono>
#include <experimental/source_location>
#include <string>
#include <type_traits>

// SFINAE test
template <typename T>
class __has_to_json
{
private:
    typedef char YesType[1];
    typedef char NoType[2];

    template <typename C> static YesType& test( decltype(&C::to_json) ) ;
    template <typename C> static NoType& test(...);
public:
    enum { value = sizeof(test<T>(0)) == sizeof(YesType) };
};



/**
 * Converts an object with a `to_json` method to
 * its JSON representation
 */
template<
    typename T,
    typename = typename std::enable_if<__has_to_json<T>::value, std::string>::type
>
const std::string to_json(const T& h) {
    return h.to_json();
}



template<
    typename T,
    typename = typename std::enable_if<std::is_arithmetic<T>::value, T>::type
> const std::string to_json(const T h) {
    return std::to_string(h);
}

const std::string to_json(const char *str);
const std::string to_json(const std::string &str);
const std::string to_json(const std::chrono::system_clock::time_point &timestamp);
const std::string to_json(const std::experimental::source_location &loc);

template<typename T> const std::string to_json_array_helper(const T head) {
    return to_json(head);
};

template<typename T, typename ...Args> const std::string to_json_array_helper(const T head, const Args ...tail) {
    return to_json(head) + "," + to_json_array_helper(tail...);
};

template<typename ...Args> const std::string to_json_array(const Args ...lst) {
     return "[" + to_json_array_helper(lst...) + "]";
}


#endif // JSON_H_INCLUDED
