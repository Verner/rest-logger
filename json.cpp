#include "json.h"

const std::string to_json(const char *str) {
    return '"'+std::string(str)+'"';
}

const std::string to_json(const std::string &str) {
    return '"'+str+'"';
}

const std::string to_json ( const std::chrono::system_clock::time_point& timestamp )
{
    return std::to_string(timestamp.time_since_epoch().count());
}

const std::string to_json ( const std::experimental::source_location& loc )
{
    return "{\"file\":\"" + std::string(loc.file_name()) +
           "\",\"line\":" + std::to_string(loc.line()) +
           ",\"func\":\"" + std::string(loc.function_name())+"\"}";
}


