#include "logger.h"
#include "rest_logger.h"

#include <iostream>

using namespace verner::logging;

class test_str {
public:
    int a, b, c;
    test_str(int a_, int b_, int c_): a(a_), b(b_), c(c_) {};
    std::string to_json() const {
        return "{\"a\":"+std::to_string(a)+",\"b\":"+std::to_string(b)+",\"c\":"+std::to_string(c)+"}";
    }
};

int main(int argc, char **argv) {
    Config cfg{
        {"url", "http://localhost:8080/logs"},
        {"user", "admin"},
        {"password", "secret"},
        {"iterations", "100000"},
        {"loggers", "1"}
    };
    for(int i=1; i < argc; i++) {
        if ( std::string(argv[i]).rfind("--") == 0 ) {
            auto key = std::string(argv[i]+2);
            auto val = std::string(argv[i+1]);
            cfg[key] = val;
            i++;
        }
    }
    test_str data {1,2,3};
    Logger<RESTLogger> l {cfg};
    int iterations = std::stoi(cfg["iterations"]);
    int loggers = std::stoi(cfg["loggers"]);
    std::vector<std::thread> log_threads;
    for(int logger=0;logger<loggers;logger++) {
        log_threads.push_back(std::thread([&]() {
            for(int i=0; i<iterations; i++) {
                l.log(INFO, l.loc(), "logger", logger, "iteration", i, std::to_string(i), data);
            }
        }));
    }
    for(auto &th: log_threads) {
        th.join();
    }
    std::cout << "Processed " << iterations*loggers << " events." << std::endl;
}
