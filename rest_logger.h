#ifndef REST_LOGGER_H_INCLUDED
#define REST_LOGGER_H_INCLUDED

#include "json.h"
#include "logger.h"

#include "concurrentqueue.h"

#include <fstream>
#include <string>
#include <thread>
#include <vector>

namespace verner {
namespace logging {



class RESTLogger {

private:
    moodycamel::ConcurrentQueue<std::string> *log_queue;
    std::vector<std::thread> worker_threads;
    std::atomic<bool> stop_scheduled;
    std::string log_server_url, log_server_user, log_server_password;
    std::atomic<size_t> approx_queue_size = 0;

    std::ofstream log_file;

    int BATCH_SIZE = 10000;
    size_t MAX_MEM_SIZE = 10000000;
    int NUM_WORKERS = 7;
    bool ASYNC_POST = true;

    void process_queue();
    void process_item(const std::string& json_log_item);
    void post_request(const std::string& body, bool force_async = false);

public:
    RESTLogger(const Config& cfg);
    ~RESTLogger();

    template<typename ...Args> void _log(const level l,
                                        std::experimental::source_location location,
                                        const std::chrono::system_clock::time_point& ts,
                                        const Args ...data
                                       )
    {
        std::string json_log_item = "{\"ts\":" + to_json(ts) +
                               ",\"lvl\":" + to_json((int)l) +
                               ",\"loc\":" + to_json(location) +
                               ",\"args\":"+ to_json_array(data...)+"}";
        if (approx_queue_size < MAX_MEM_SIZE) {
            log_queue->enqueue(json_log_item);
            approx_queue_size += json_log_item.size();
        } else {
            process_item(json_log_item);
        }
    }
};

}
}


#endif // REST_LOGGER_H_INCLUDED
