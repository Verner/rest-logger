#ifndef LOGGER_H_INCLUDED
#define LOGGER_H_INCLUDED

#include <chrono>
#include <experimental/source_location>
#include <unordered_map>

namespace verner {
namespace logging {

enum level {
        DEBUG = 0,
        INFO = 1,
        WARNING = 2,
        ERROR = 3,
        CRITICAL = 4
};

typedef std::unordered_map<std::string,std::string> Config;

template<typename LogImpl>
class Logger {

private:
    LogImpl _logger;

public:

    Logger(const Config& cfg): _logger(cfg) {};

    /**
     * @Returns when called without parameters, returns a string containing the source code location of the calling code.
     */
    constexpr static std::experimental::source_location loc(std::experimental::source_location location = std::experimental::source_location::current()) {
        return location;
    }

    /**
     * Logs an event with the logger.
     *
     * @param level    ... the log level of the event
     * @param location ... the location where the event was generated (use Logger<>::loc())
     * @param data     ... a list of arguments which will be saved with the event
     */
    template<typename ...Args> void log(const level l, std::experimental::source_location location, const Args ...data) {
        _logger._log(l, location, std::chrono::system_clock::now(), data...);
    };

};


}
}



#endif // LOGGER_H_INCLUDED
