install-deps:
	./vcpkg/bootstrap-vcpkg.sh
	./vcpkg/vcpkg install @dependencies.txt
log-server:
	docker-compose up -d
	curl -X PUT --user "admin:secret" http://localhost:8080/logs
